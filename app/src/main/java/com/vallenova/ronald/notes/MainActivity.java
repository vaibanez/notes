package com.vallenova.ronald.notes;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class MainActivity extends Activity {
    // This is the method that is called when the submit button is clicked
    public final static String apiURL = "https://pruebaapi.herokuapp.com/api/v1/notes";
    public final static String EXTRA_MESSAGE = "com.vallenova.ronald.notes.MESSAGE";

    private class CallAPI extends AsyncTask<String, String, String> {
        @Override
        protected String doInBackground(String... params) {
            String urlString=params[0];
            String resultToDisplay = "";
            InputStream in = null;

            RadioGroup rg = (RadioGroup) findViewById(R.id.grupo_radio);
            RadioButton rd = ((RadioButton)findViewById(rg.getCheckedRadioButtonId() ));
            EditText titleEditText = (EditText) findViewById(R.id.title_note);
            EditText bodyEditText = (EditText) findViewById(R.id.body_note);
            EditText categoryEditText = (EditText) findViewById(R.id.category_note);
            EditText idEditText = (EditText) findViewById(R.id.id_note);

            String id_nota = idEditText.getText().toString();
            String titulo_nota = titleEditText.getText().toString();
            String descripcion_nota = bodyEditText.getText().toString();
            String categoria_nota = categoryEditText.getText().toString();

            switch(rd.getText().toString())
            {
                case "Listar Notas":
                    //HTTP Get
                    try {
                        URL url = new URL(urlString);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        in = new BufferedInputStream(urlConnection.getInputStream());
                        if(in != null)
                            resultToDisplay = convertInputStreamToString(in);
                        else
                            resultToDisplay = "No trabaja!";
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        return e.getMessage();
                    }
                    break;
                case "Mostrar Nota":

                    //HTTP Get
                    try {
                        URL url = new URL(urlString + "/" + id_nota);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        in = new BufferedInputStream(urlConnection.getInputStream());
                        if(in != null)
                            resultToDisplay = convertInputStreamToString(in);
                        else
                            resultToDisplay = "No trabaja!";
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        return e.getMessage();
                    }
                    break;
                case "Crear Nota":
                    //HTTP Post
                    try {
                        URL url = new URL(urlString);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("POST");
                        urlConnection.setDoInput(true);
                        urlConnection.setDoOutput(true);

                        String urlParameters = "note[title]="+titulo_nota+"&note[body]="+descripcion_nota+"&note[category_id]="+categoria_nota;

                        // Enviando post
                        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                        wr.writeBytes(urlParameters);
                        wr.flush();
                        wr.close();

                        in = new BufferedInputStream(urlConnection.getInputStream());
                        if(in != null)
                            resultToDisplay = convertInputStreamToString(in);
                        else
                            resultToDisplay = "No trabaja!";
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        return e.getMessage();
                    }
                    break;
                case "Actualizar Nota":
                    //HTTP Put
                    try {

                        URL url = new URL(urlString + "/" + id_nota);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("PUT");
                        urlConnection.setDoInput(true);
                        urlConnection.setDoOutput(true);

                        String urlParameters = "note[title]="+titulo_nota+"&note[body]="+descripcion_nota+"&note[category_id]="+categoria_nota;

                        // Enviando PATCH
                        DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                        wr.writeBytes(urlParameters);
                        wr.flush();
                        wr.close();

                        in = new BufferedInputStream(urlConnection.getInputStream());
                        if(in != null)
                            resultToDisplay = convertInputStreamToString(in);
                        else
                            resultToDisplay = "No trabaja!";
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        return e.getMessage();
                    }
                    break;
                case "Borrar Nota":
                    //HTTP Patch
                    try {

                        URL url = new URL(urlString + "/" + id_nota);
                        HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                        urlConnection.setRequestMethod("DELETE");
                        //urlConnection.setDoInput(true);
                        //urlConnection.setDoOutput(true);

                        // Enviando PATCH
                        //DataOutputStream wr = new DataOutputStream(urlConnection.getOutputStream());
                        ///wr.flush();
                        //wr.close();

                        in = new BufferedInputStream(urlConnection.getInputStream());
                        if(in != null)
                            resultToDisplay = convertInputStreamToString(in);
                        else
                            resultToDisplay = "No trabaja!";
                    } catch(Exception e) {
                        System.out.println(e.getMessage());
                        return e.getMessage();
                    }
                    break;
            } // end switch



            return resultToDisplay;
        }

        protected void onPostExecute(String result) {
            Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
            intent.putExtra(EXTRA_MESSAGE, result);

            startActivity(intent);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private static String convertInputStreamToString(InputStream inputStream) throws IOException{
        BufferedReader bufferedReader = new BufferedReader( new InputStreamReader(inputStream));
        String line = "";
        String result = "";
        while((line = bufferedReader.readLine()) != null)
            result += line;

        inputStream.close();
        return result;

    }

    public void getNotes(View view) {
        String urlString = apiURL;
        new CallAPI().execute(urlString);
    }
}
